"""Module for housing different imput methods.

The most common one is human, which handles human input via UI.
"""

import logging
from random import choice
from copy import deepcopy

from gamelogic import plant_seeds

def human(
   player: int,
   ui: 'display.terminal.ui', 
   log: int=0,
   **kwargs: dict) -> int:
    """Asks a human for an index and returns it.
    
    It is not guaranteed to be valid, another process will need
    to check that.
    
    Args:
        player: The player choosing the index.
           exclusively used for logging.
        ui: The ui that will ask the player. The ui already has
           a reference to the board through its board_drawer attribute.
        log: Integer representing how much should be logged.
    """

    index = ui.status_drawer.get_house_from_player()
    
    if log >= 4:
        logging.info(
           'Player {0}\'s humanly chosen index: {1}.'.format(player, index))
    
    return index
    
def random(
   board: 'board.Board', 
   player: '1 or 2', 
   log: int=0,
   **kwargs: dict) -> int:
    """Randomly picks a valid index.
    
    Args:
        board: The board to pick a house from.
        player: The player picking the house.
        log: Integer representing how much should be logged.
    """

    valid_houses = board.get_valid_houses(player)
    index = choice(valid_houses).get_index()
    
    if log >= 4:
        logging.info(
           'Player {0}\'s randomly chosen index: {1}.'.format(player, index))
    
    return index
    
def maximum(
   board: 'board.Board', 
   player: '1 or 2', 
   log: int=0,
   **kwargs: dict) -> int:
    """Chooses the house with the most seeds.
    
    Args:
        board: The board to pick a house from.
        player: The player picking the house.
        log: Integer representing how much should be logged.
    """
    
    houses = board.get_houses(player)
    index = max(houses).get_index()
    
    if log >= 4:
        logging.info(
           'Player {0}\'s maximum chosen index: {1}.'.format(player, index))
    
    return index

def minimum(
   board: 'board.Board', 
   player: '1 or 2',
   log: int=0,  
   **kwargs: dict) -> int:
    """Chooses the house with least seeds, but more than one.
    
    Args:
        board: The board to pick a house from.
        player: The player picking the house.
        log: Integer representing how much should be logged.
    """
    
    houses = board.get_valid_houses(player)
    index = min(houses).get_index()
    
    if log >= 4:
        logging.info(
           'Player {0}\'s minimum chosen index: {1}.'.format(player, index))
       
    return index
       
    
def minimax(
   board: 'board.Board', 
   player: '1 or 2',
   max_depth: int=5,
   log: int=0,
   **kwargs: dict) -> int:
    """Minimax algorithm assuming perfect play from both sides.
    
    This function is somewhat weak, as the opponent rarely plays perfectly.
    
    Args:
        board : The board to pick a house from.
        player: The player picking the house.
        max_depth: The maximum depth the function should reach
           before aborting. This needs to be larger than zero.
        log: Integer representing how much should be logged.
        
    Raises:
        ValueError: If max_depth is less than one.
    """
    
    maximizing = True
    
    eval_dict = _minimax(
       maximizing=maximizing,
       board=board,
       depth=0,
       max_depth=max_depth,
       player=player)
       
    val = eval_dict['val']
    index = eval_dict['index']
        
    if log >= 4:
        logging.info(
           'Player {0}\'s minimax chosen index: {1}. With evaluation: {2}.'.format(
           player, index, val))
        
    return index
        
def _minimax(
   maximizing: bool, 
   board: 'board.Board', 
   depth: int, 
   max_depth: int, 
   player: int) -> int:
    """Private recursive inner workings of the minimax function.
    
    Args:
        maximizing: Is it ´player´'s turn right now?
        board: The state of the board.
        depth: The current depth of the function.
        max_depth: The maximmum depth before aborting.
        player: The player trying to be maxed.
    """

    def evaluate(board: 'board.Board', player: int) -> int:
        """Evaluates the scores with the current board.
        
        A higher value means a better score for ´player´
        
        Args:
            board: The board in the current state.
            player: The player to be evaluated for.
        """
    
        scores = board.get_scores()
        
        if player == 1:
            return scores['player 1'] - scores['player 2']
        if player == 2:
            return scores['player 2'] - scores['player 1']

    if depth >= max_depth or board.is_game_over():
        return evaluate(board, player)
    
    eval_dict = {'index': 0}
    if maximizing:
        eval_dict['val'] = -float('inf')
        
        turn = player
        
    else:
        eval_dict['val'] = float('inf')
        
        turn = -player + 3
    
    indexes = [h.get_index() for h in board.get_valid_houses(turn)]
    
    if len(indexes) == 0:
        ev = _minimax(
           maximizing=not maximizing,
           board=board, 
           depth=depth + 1,
           max_depth=max_depth,
           player=player)
        
    for i in indexes:
        board_c = deepcopy(board)
           
        circuit = board_c.get_circuit(turn, i)
        
        plant_seeds(turn, circuit)
        
        ev = _minimax(
           maximizing=not maximizing,
           board=board_c, 
           depth=depth + 1,
           max_depth=max_depth,
           player=player)
        
        if maximizing:
            if ev > eval_dict['val']:
                eval_dict = {'val': ev, 'index': i}
            
        else:
            if ev < eval_dict['val']:
                eval_dict = {'val': ev, 'index': i}
    
    if depth == 0:
        return eval_dict
        
    else:
        return eval_dict['val']
        
input_methods = {
   'human': human,
   'random': random,
   'maximum': maximum,
   'minimum': minimum,
   'minimax': minimax}