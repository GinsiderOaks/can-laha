"""Main module for running the game."""

import sys
import logging
import argparse
from time import sleep
from random import randint, choice

from gamelogic import plant_seeds
from board import Board
import display.terminal as terminal
from inputs import input_methods
    
def get_p1_input(**kwargs):
    """Will be assigned later.
    
    If callled before assignment, raises NotImplementedError"""
    
    raise NotImplementedError
    
def get_p2_input(**kwargs):
    """Will be assigned later.
    
    If callled before assignment, raises NotImplementedError"""
    
    raise NotImplementedError
    
def get_start_turn() -> int:
    """Return who starts the game."""
    
    return 1

def get_start_house_index(
   board: Board,
   turn: int,
   ui: terminal.UI,
   log: int=0) -> 'int or None':
    """Return a valid house to start the turn with.
    
    Args:
        board: The board at its current state.
        turn: Who's turn it currently is.
        ui: The ui for human interaction.
        log: Integer representing how much should be logged.
        
    Returns:
        Integer representing the index of the house,
        or None representing no valid choice possible.
    """
    
    house_count = board.get_player_house_count()
    
    inp = None
    
    valid_choices = board.get_valid_houses(turn)
    
    if len(valid_choices) == 0:
        if log >= 3:
            logging.info('No valid choices available. Passing over turn.')
            
        return None
        
    elif len(valid_choices) == 1:
        index = valid_choices[0].get_index()
    
        if log >= 3:
            logging.info('Only one valid index. Automatically choosing index: {0}'.format(
               index))
        
        return index
        
    get_input = get_p1_input if turn == 1 else get_p2_input
    
    while True:
        inp = get_input(board=board, player=turn, ui=ui, log=log)
        
        if inp == 'h' and ui is not None:
            ui.board_drawer.draw_help(turn)
            ui.board_drawer.draw_board()
            continue
        
        # Attempts to turn the input into an integer
        try:
            inp = int(inp)
        
        # It might fail, for example a string that does not convert
        except (ValueError, TypeError):
            continue
            
        # Attempts to access the house.
        try:
            house = board.get_house(turn, inp)
        except IndexError:
            continue
        
        if house in valid_choices:
            if log >= 3:
                logging.info('The index: {0} was accepted.'.format(house.get_index()))
            break

    return house.get_index()
    
def main_loop(board: 'board.Board', ui: 'terminal.UI', wait: float=.2, log: int=0):
    """The main loop of the game.
    
    Takes care of handling who'se turn it is,
    and user input.
    
    Args:
        board: The board of the game.
        ui: The user interface.
        wait: How long should be pause
        log: Integer representing how much should be logged.
    """

    # Who's turn is it?
    turn = get_start_turn()
    
    # Enters the main loop
    # The game continues as long as there are seeds in the houses.
    while not board.is_game_over():
    
        if log >= 3:
            logging.info('Player {0}\'s turn.'.format(turn))
        
        if ui is not None:
            ui.status_drawer.draw_player_turn(turn)
        
        house_index = get_start_house_index(board=board, turn=turn, ui=ui, log=log)
        if house_index is None:
            turn = -turn + 3
            continue
        
        if ui is not None:
            ui.status_drawer.draw_hand(0)
            ui.pause(wait=wait)
        
        circuit = board.get_circuit(turn, house_index)
        plant_seeds(turn, circuit, wait=wait, ui=ui)
        
        turn = -turn + 3
        
    # At the end of the game loop, the game ends.
    end_game(board, ui, log=log)
    
def end_game(board: Board, ui: terminal.UI, log: int=0):
    """End the game and display a winner (or tie).
    
    Args:
        board: The game board (to get the scores).
        ui: Used to draw the score.
        log: Integer representing how much should be logged.
    """

    scores = board.get_scores()

    if scores['player 1'] > scores['player 2']:
        winner = 1
        
    elif scores['player 1'] < scores['player 2']:
        winner = 2
        
    # Tie
    else:
        winner = None
        
    if log > 0:
        if winner is None:
            logging.info('It is a tie with {0} points to each.'.format(
               scores['player 1']))
            
        else:
            logging.info('Player {0} wins {1} to {2}.'.format(
               winner,
               *sorted(tuple(scores.values()), reverse=True)))
    
    if ui is not None:
        ui.draw_winner(winner, scores)
        ui.pause(wait=None)
        terminal.end()
        
def add_args(parser: argparse.ArgumentParser):
    """Add arguments to the argparser.
    
    Args:
        parser: The argument parser
    """
    
    def wait_type(x):
        """Type that ensures ´wait´ arg will be a float greater or equal to zero."""
        x = float(x)
        if x < 0:
            raise argparse.ArgumentTypeError('Minimum wait is 0.')
        return x
        
    def playerhouses_type(x):
        """Type that ensures ´playerhouses´ arg will be an int greater than zero."""
        x = int(x)
        if x <= 0:
            raise argparse.ArgumentTypeError('Minimum player houses is 1.')
        return x
        
    def houseseeds_type(x):
        """Type that ensures ´houseseeds_type´ arg will be an int greater than zero."""
        x = int(x)
        if x <= 0:
            raise argparse.ArgumentTypeError('Minimum house seeds is 1.')
        return x
    
    parser.add_argument('--noui', 
       action='store_true',
       default=False)
    parser.add_argument('--player1', 
       choices=list(input_methods.keys()),
       default='human')
    parser.add_argument('--player2', 
       choices=list(input_methods.keys()),
       default='random')
    parser.add_argument('-w', '--wait', 
       type=wait_type,
       default=wait_type(.2))
    parser.add_argument('-l', '--log', 
       action='count', 
       default=0)
    parser.add_argument('--playerhouses',
       type=playerhouses_type,
       default=playerhouses_type(4))
    parser.add_argument('--houseseeds',
       type=houseseeds_type,
       default=houseseeds_type(4))

def main():
    """Setup for the game."""
    
    global get_p1_input, get_p2_input
    
    parser = argparse.ArgumentParser(
       prog='game.py',
       description='Arguments for the calaha game.')
       
    add_args(parser)
    args = parser.parse_args()

    board = Board(
       player_house_count=int(args.playerhouses), 
       house_seeds=int(args.houseseeds))
    
    # integer representing things to log.
    # log <= 0: Log nothing.
    # log = 1: Log conclusion of game.
    # log = 2: Log setup of game.
    # log = 3: Log choices done during game.
    # log >= 4: Log additional events during game.
    
    log = args.log
       
    if log > 0:
        logging.basicConfig(filename='can-laha.log', level=logging.DEBUG)
        
        phc = board.get_player_house_count()
        shs = board.get_house_start_seeds()
        
        if log >= 2:
            logging.info('Board set up with {0} houses for each player and \
{1} seeds in each house.'.format(phc, shs))
    
    ui = None
    
    if not args.noui:
        stdscr = terminal.initialize()
        ui = terminal.UI(stdscr, board)
        
        ui.board_drawer.draw_board()
    
    get_p1_input = input_methods[args.player1]
    get_p2_input = input_methods[args.player2]
    
    wait = float(args.wait)
    
    # Begins the main loop of the game.
    main_loop(board=board, ui=ui, wait=wait, log=log)

if __name__ == '__main__':
    main()