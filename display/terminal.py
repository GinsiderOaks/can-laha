"""Display the kalaha game in a terminal.

Classes:
    BoardDrawer: Responsible for drawing the board
    StatusDrawer: Responsible for displaying the status of the game,
       and asking the user prompts during the game.
    UI: Responsible for housing a instance of BoardDrawer and StatusDrawer,
       as well as displaying who won the game and such.
"""

from math import ceil
from unicurses import *

HIGHLIGHT_ATTR = A_BOLD

class BoardDrawer:
    """Class for drawing a kalaha board in the terminal.
    
    Class methods:
        get_max_seed_digits (static method): Get the maximum digits of seeds.
        get_board_width (class method): Get the width of a board (shell included).
        draw_board: Draws the board with normal colors and attributes.
        draw_help: Draws a version of the board with helping colors and indexes.
        draw_shell: Draws the shell around the board.
        draw_store: Draw a store belonging to a player.
        draw_house: Draw a house belonging to a player with a specific index.
        draw_storage: Draw either a store or house.
    """
    
    SHELL_LS = '#'
    SHELL_RS = '#'
    SHELL_TS = '#'
    SHELL_BS = '#'
    SHELL_TL = '#'
    SHELL_BL = '#'
    SHELL_TR = '#'
    SHELL_BR = '#'
    
    STORE_LS = 65656 #'│'
    STORE_RS = 65656 #'│'
    STORE_TS = 65649 #'─'
    STORE_BS = 65649 #'─'
    STORE_TL = 65644 #'┌'
    STORE_BL = 65645 #'└'
    STORE_TR = 65643 #'┐'
    STORE_BR = 65642 #'┘'
    
    LEFT_HOUSE = '('
    RIGHT_HOUSE = ')'
    
    SEED_FILL = ' '
    
    BOARD_HEIGHT = 5
    
    def __init__(
       self, board: 'Board', win: 'unicurses window'):
        """Initialize BoardDrawer instance.
        
        Args:
            board: The board to be drawn.
            win: The unicurses window to draw on.
        """
        
        self.board = board
        self.win = win
        self.highlight_attr = HIGHLIGHT_ATTR
        
        self._init_store_wins()
        
    def _init_store_wins(self):
        """Setup a window for each store."""
    
        win = self.win
        
        bw = self._get_board_width()
        msd = self._get_max_seed_digits()
    
        self.store_win1 = \
            win.subwin(3, msd + 2, 1, bw - (msd+3))
            
        self.store_win2 = \
            win.subwin(3, msd + 2, 1, 1)
        
    def _get_max_seed_digits(self) -> int:
        """Return the digits of the total amount of seeds."""
        return self.get_max_seed_digits(self.board)
        
    @staticmethod
    def get_max_seed_digits(board) -> int:
        """Return the digits of the total amount of seeds."""
        max_seeds = board.get_max_seeds()
        
        return len(str(max_seeds))
        
    def _get_board_width(self) -> int:
        """Return the width of the board in characters including borders."""
        return self.get_board_width(self.board)
        
    @classmethod
    def get_board_width(cls, board) -> int:
        """Return the width of the board in characters including borders."""
        player_house_count = board.get_player_house_count()
    
        max_seed_digits = cls.get_max_seed_digits(board)
        width = 2 + (max_seed_digits+2) * (player_house_count+2)
        
        return width
        
    def _get_store_position(self, player: int) -> '(y, x) coordinates':
        """Gets the (y, x) position of where to draw a store.
        To get the coordinates of the store contents, the y- and x-
        coordinate should be incremented by one.
        
         Args:
            player: Either a 1 or 2, representing p1 or p2 respectively.
        """
        
        y, x = 1, 0
        
        # If the store belongs to player 1
        if player == 1:
            # Calculate the x position of the store, 
            # so it touches the right side of the shell
            max_seed_digits = self._get_max_seed_digits()
            board_width = self._get_board_width()
            
            x += board_width - max_seed_digits - 3
            
        # Else it belongs to player 2
        else:
            # Simply moves the store one to the right
            x += 1
            
        return (y, x)
        
    def _get_house_position(self, player: int, index: int) -> '(y, x) coordinates':
        """Gets the (y, x) position of where to draw a house.
        To get the coordinates of the house contents the x-
        coordinate should be increment by one.
        
         Args:
            player: Either a 1 or 2, representing p1 or p2 respectively.
            index: The index of the store.
        """
        
        max_seed_digits = self._get_max_seed_digits()
        
        y, x = 1, max_seed_digits + 3
        
        
        # If the house belongs to player 1
        if player == 1:
            # Moves the house down by 2
            y += 2
            # Moves the house right depending on the index
            x += (max_seed_digits + 2) * index
          
        # Else it belongs to player 2
        else:
            # Moves the house left depending on the index
            player_house_count = self.board.get_player_house_count()
            x += (max_seed_digits + 2) * (player_house_count - index - 1)
            
        return (y, x)
        
    def _align_seeds(self, seeds: int, align_str: str='>') -> str:
        """Left-aligns a seed value with padding to the right.
        
        The amount of padding plus the length of the seeds string
        equals the max seed digits, so the seed-field will override 
        old values that had more digits.
        
        Args:
            seeds: The seed value to be aligned.
            align_str: The string used to determine what alignment should be used,
                I.E. '>' is right align, '<' is left and '^' is center.
        """
        
        msd = self._get_max_seed_digits()
        fill = self.SEED_FILL
        
        return '{{:{0}{1}{2}}}'.format(fill, align_str, msd).format(seeds)
        
    def draw_board(self):
        """Draws an empty board in 3 steps:
        
        First draws the shell (the borders around the board).
        Then draws the store and then houses for player 1.
        Lastly draws the store and then houses for player 2
        """
        
        self.draw_shell()
        
        board = self.board
        
        player_house_count = board.get_player_house_count()
        
        # Draw for player 1 and 2
        for p in range(1, 3):
            store = board.get_store(p)
            self.draw_store(store)
            for i in range(player_house_count):
                house = board.get_house(p, i)
                self.draw_house(house)
                
        self.win.refresh()
        
    def draw_help(self, player: int):
        """Draws a board with helping colors and indexes.
        
        The seed count of the houses will be replaced with the respective indexes,
        and the houses that are valid (belongs to the player and has > 0 seeds) will
        be colored green.
        
        Args:
            player: The player who should recieve the help.
        """
        
        board = self.board
        
        help_board = board.get_help_board()
        help_board_drawer = BoardDrawer(help_board, self.win)
        
        player_house_count = help_board.get_player_house_count()
        
        help_board_drawer.draw_shell()
        # Draw for player 1 and 2
        for p in range(1, 3):
            store = help_board.get_store(p)
            help_board_drawer.draw_store(store, c_pair=color_pair(4))
            for i in range(player_house_count):
                help_house = help_board.get_house(p, i)
                house = board.get_house(p, i)
                
                pair_index = 3 if house.get_seeds() > 0 and p == player else 4
                help_board_drawer.draw_house(help_house, c_pair=color_pair(pair_index))
                
        help_board_drawer.win.refresh()
        getch()
        
    def draw_shell(self):
        """Draws the shell (border) around the board"""
        
        win = self.win
        
        ls = self.SHELL_LS
        rs = self.SHELL_RS
        ts = self.SHELL_TS
        bs = self.SHELL_BS
        tl = self.SHELL_TL
        tr = self.SHELL_TR
        bl = self.SHELL_BL
        br = self.SHELL_BR
        
        win.border(ls,rs,ts,bs,tl,tr,bl,br)
           
    def draw_store(
       self, 
       store: 'Board.Store', 
       c_pair: int=None, 
       highlighted: bool=False):
        """Draws a store belonging to either player 1 or 2.
        
        Args:
            store: The store object to be drawn.
            c_pair: Optional color pair to override the player color pair.
            highlighted: Should the store be highlighted?
        """
        
        player = store.get_player()
        
        # The attribute and color
        attribute = c_pair if c_pair is not None else color_pair(player)
        attribute = attribute | (self.highlight_attr * highlighted)
        
        ls = self.STORE_LS
        rs = self.STORE_RS
        ts = self.STORE_TS
        bs = self.STORE_BS
        tl = self.STORE_TL
        tr = self.STORE_TR
        bl = self.STORE_BL
        br = self.STORE_BR
        
        if player == 1:
            store_win = self.store_win1
        elif player == 2:
            store_win = self.store_win2
        
        store_win.border(
           ls,
           rs,
           ts,
           bs,
           tl,
           tr,
           bl,
           br)
           
        store_seeds = store.get_seeds()
        seeds_str = self._align_seeds(store_seeds)
        store_win.addstr(1, 1, seeds_str, attribute)
        
        store_win.refresh()
        
    def draw_house(
       self, 
       house: 'Board.House', 
       c_pair: int=None, 
       highlighted: bool=False):
        """Draws a house belonging to either player 1 or 2, and with a specific index.
        
        Args:
            house: The house object to be drawn.
            c_pair: Optional color pair to override the player color pair.
            highlighted: Should the store be highlighted?
        """
        
        player = house.get_player()
        index = house.get_index()
        
        # Gets the house position
        house_position = self._get_house_position(player, index)
        y = house_position[0]
        x = house_position[1]
        
        # The attribute and color
        attribute = c_pair if c_pair is not None else color_pair(player)
        attribute = attribute | (self.highlight_attr * highlighted)
        
        max_seed_digits = self._get_max_seed_digits()
        win = self.win
        
        left_house = self.LEFT_HOUSE
        right_house = self.RIGHT_HOUSE
        house_seeds = self.board.get_house(player, index).get_seeds()
        seeds_str = self._align_seeds(house_seeds)
        
        win.addch(y, x, left_house, attribute)
        x += 1
        win.addstr(y, x, seeds_str, attribute)
        x += max_seed_digits
        win.addch(y, x, right_house, attribute)
        
        win.refresh()
        
    def draw_storage(
       self, 
       storage: 'Board.Store or Board.House',
       c_pair: int=None,
       highlighted: bool=False):
        """Draws a storage unit, either a house oor store.
        
        Args:
            storage: Either a house or store to be drawn.
            c_pair: Optional color pair to override the player color pair.
            highlighted: Should the storage be highlighted?
        """
        
        try:
            # Attempts to get the index of the storage.
            storage.get_index()
            
        except AttributeError:
            # If it does not have an index, it is a store.
            self.draw_store(
               storage, 
               c_pair=c_pair, 
               highlighted=highlighted)
            
        else:
            # If it does have an index, it is a house.
            self.draw_house(
               storage, 
               c_pair=c_pair, 
               highlighted=highlighted)

class StatusDrawer:
    """Class for drawing the state of the game and prompting the player.
    
    Class methods:
        draw_player_turn: Draws who'se turn it is, in their color.
        draw_hand: Draws how many seeds the hand contains.
        get_house_from_player: Asks the player for a house index,
           and returns it. It is up to an external program to determine
           if the index is valid.
    """
    
    STATUS_HEIGHT = 10
    
    def __init__(self, win: 'unicurses window'=None):
        """Initiate StatusDrawer instance.
        
        Args:
            win: The window to use.
        """
        
        self.win = win
       
        
    def draw_player_turn(self, player: int):
        """Draws who'se turn it is on screen.
        
        Args:
            player: Either a 1 or 2, representing p1 or p2 respectively.
        """
        
        win = self.win
        
        y, x = 0, 0
        
        attribute = color_pair(player)
        
        string = 'Player {}\'s turn.'.format(player)
        win.move(y, x)
        win.clrtoeol()
        win.addstr(string, attribute)
        
        win.refresh()
            
    def draw_hand(self, hand: int):
        """Draws the amount of seeds in the hand.
        
        Args:
            hand: Integer representing the seeds in the hand.
        """
        
        win = self.win
        
        y, x = 1, 0
        
        string = 'Hand: {} seeds.'.format(hand)
        win.move(y, x)
        win.clrtoeol()
        win.addstr(string)
        
        win.refresh()
        
    def get_house_from_player(self):
        """Prompts the player to type a valid house index and returns the answer.
        
        The return value is not guaranteed to be valid, another process has to
        check that.
        """
    
        win = self.win
        
        y, x = 2, 0
        
        prompt = 'Please type a valid house index: '
        win.move(y, x)
        win.clrtoeol()
        
        
        win.addstr(prompt)
        echo()
        player_input = str(win.getstr(), 'utf-8')
        noecho()
        win.move(y, x)
        win.clrtoeol()
        win.refresh()
        return player_input
            
class UI:
    """Class for displaying the user interface.
    
    Class methods:
        pause: Pauses the game for ´wait´ time, or until a button has been pressed.
           if `wait` is None, it waits indefinately.
        draw_winner: Displays who has won the game.
    """

    UI_HEIGHT = 10

    def __init__(self, stdscr: 'unicurses stdscr', board: 'Board'):
        """Initialize UI instance.
        
        Args:
            stdscr: The standard screen returned by initscr()
            board: The board to be attached.
        """
    
        self.win = stdscr
        self.highlight_attr = HIGHLIGHT_ATTR
        
        board_height = BoardDrawer.BOARD_HEIGHT
        board_width = BoardDrawer.get_board_width(board)
        
        board_drawer_win = \
           stdscr.subwin(board_height,board_width,0,0)
              
           
        status_drawer_win = \
           stdscr.subwin(board_height,0)
        
        self.board_drawer = BoardDrawer(board, board_drawer_win)
           
        self.status_drawer = StatusDrawer(status_drawer_win)
       
    def pause(self, wait: float=.2):
        """Pauses for ´wait´ seconds, or until the user presses a button.
        
        If wait is None, waits until the user presses a button.
        
        Args:
            wait: The amount of seconds to wait. This will be rounded to tenths,
                so it is not wholly precise. Must be grater or equal to zero.
                
        Raises:
            ValueError: If ´wait´ is less than zero.
        """
        
        # If wait is None, should wait until user presses a button.
        if wait is None:
            getch()
        
        # If it instead is larger than 0, should wait for (approx) that amount.
        elif wait > 0:
            tenths = int(ceil(wait * 10))
            halfdelay(tenths)
            self.win.getch()
            nocbreak()
        
        # If it is excactly 0, should not wait at all.
        elif wait == 0:
            return
        
        # Otherwise wait is negative, in which case an error should be raised.
        else:
            raise ValueError('Argument ´wait´ mustn\'t be lesser than 0.')
        
    def draw_winner(self, winner: 'int or None', scores: dict):
        """Clears the entire screen and displays the winner.
        
        Args:
            winner: Either a 1, 2 or None representing p1 winning, p2 winning or a tie.
        """
        win = self.win
        
        win.clear()
        
        attribute = HIGHLIGHT_ATTR
        
        if winner is None:
            win.addstr('It\'s a tie with {} points to each.'.format(
               tuple(scores.values())[0]), attribute)
            
        else:
            message = 'Player {0} wins {1} to {2}.'.format(
               winner,
               *sorted(tuple(scores.values()), reverse=True))
               
            attribute = attribute | color_pair(winner)
            
            win.addstr(message, attribute)
        
def initialize():
    """Initializes the terminal, most importantly calling initscr."""

    global HIGHLIGHT_ATTR

    stdscr = initscr()
    noecho()
    cbreak()
    
    start_color()
    curs_set(False)
    
    p1_colors = (COLOR_RED, COLOR_BLACK)
    p2_colors = (COLOR_BLUE, COLOR_BLACK)
    highlight_colors = (COLOR_GREEN, COLOR_BLACK)
    lowlight_colors = (8, COLOR_BLACK) # Grey fg color.
    
    init_pair(1, *p1_colors)
    init_pair(2, *p2_colors)
    init_pair(3, *highlight_colors)
    init_pair(4, *lowlight_colors)
    
    return stdscr
    
def end():
    """Ends the terminal, most  importantly calling endwin."""
    
    clear()
    nocbreak()
    echo()
    endwin()