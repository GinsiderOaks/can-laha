"""Module for handling a kalaha board. 

Classes:
    Board: Class for keeping kalaha-board data and moving the seeds around
"""

from math import floor, log
from collections import deque


class Board:
    """Class implementing a kalaha board.
    
    A kalaha-board conceptually consists of 3 unique components:
        Two stores, one for each player. The stores keep the points for each player.
        An arbitrary amount of stores, same for each player. The houses holds seeds.
        An arbitrary amount of seeds, that get moved around the houses and put in the stores.
        
    Classes:
        House: Implementation of a kalaha house.
        Store: Implementation of a kalaha store.
    
    Class methods:
        get_player_house_count: Get the amount of houses for each player 
           (same for both players).
        get_house_start_seeds: Get the amount of seeds each house started with 
           (same for all houses).
        get_house_seeds: Get the sum of seeds in the houses.
        get_store: Get the amount of seeds in one of the players' stores.
        get_houses: Get a list of houses belonging to `player`
        get_house: Get the amount of seeds in a specific house.
        get_circuit: Get the circuit the player's hand should move around.
        get_max_seeds: Calculate total amount of seeds on the board.
        get_help_board: Get a board with same house count, suitable for help.
    """
    
    def __init__(self, player_house_count: int=6, house_seeds: int=6):
        """Initialize board instance.
        
        Args:
            player_house_count: The amount of houses for each player
            house_seeds: The amount of seeds in each house (stores excluded)
        """
    
        self._player_houses = player_house_count
        self._house_seeds = house_seeds
        
        self._p1_store = self.Store(1)
        self._p2_store = self.Store(2)
        self._stores = [self._p1_store, self._p2_store]
        
        self._p1_houses = [self.House(house_seeds, 1, i) for i in range(player_house_count)]
        self._p2_houses = [self.House(house_seeds, 2, i) for i in range(player_house_count)]
        self._houses = self._p1_houses + self._p2_houses
        
    class Storage:
        """Abstract base class for both stores and houses.
        
        Class methods:
            __int__: Return the amount of seeds in the storage as an int.
            __float__: Return the amount of seeds in the storage as an float.
            __comp__: Compare the storage to other storage units, or numeric types
            __lt__: Is the seed count in this storage lesser than 
               the one another storage or a numeric type.
            __gt__: Is the seed count in this storage greater than 
               the one another storage or a numeric type.
            plant: Plant ´amount´ of seeds in the storage from a hand.
            get_seeds: Return the amount of seeds in the storage.
            get_player: Return which player this storage belongs to.
            reset: Reset the seed-count of the storage to 0.
        """
        
        def __int__(self):
            """Return amount of seeds in store."""
            return int(self.get_seeds())
        
        def __float__(self):
            """Return the amount of seeds in the store as a float"""
            return float(self.get_seeds())
            
        def __cmp__(self, other):
            f_self = float(self)
            f_other = float(other)
            
            return f_self - f_other
            
        def __lt__(self, other):
            return self.__cmp__(other) < 0
            
        def __gt__(self, other):
            return self.__cmp__(other) > 0
        
        def plant(self, hand: int, amount: int=1) -> int:
            """Plant ´amount´ of seeds from ´hand´, and return what is
            left in ´hand´. If ´amount´ is larger than ´hand´, exhausts ´hand´.
            
            Args:
                hand: The amount of seeds to plant from.
                amount: The target amount to plant.
            """
        
            planting = min(hand, amount)
        
            self._seeds += planting
            return hand - planting
            
        def get_seeds(self):
            """Return amount of seeds in store."""
            return self._seeds
           
        def get_player(self):
            """Return which player this store belongs to."""
            return self._player
        
        def reset(self):
            """Set amount of seeds in store to zero."""
            self._seeds = 0
        
    class Store(Storage):
        """Class implementing a kalaha store.
        
        The job of the store is to keep a player's points.
        Seeds can be deposited by the ´plant´ method,
        but you can not take seeds from a store, only reset it.
        
        Class methods:
            __int__: Return the amount of seeds in the store as an int.
            __float__: Return the amount of seeds in the store as an float.
            __comp__: Compare the store to other storage units, or numeric types.
            __lt__: Is the seed count in this store lesser than 
               the one another storage or a numeric type.
            __gt__: Is the seed count in this store greater than 
               the one another storage or a numeric type.
            plant: Plant ´amount´ of seeds in the store from a hand.
            get_seeds: Return the amount of seeds in the store.
            get_player: Return which player this store belongs to.
            reset: Reset the seed-count of the store to 0.
        """
        
        def __init__(self, player: int):
            """Initialize store instance with zero seeds.
            
            Args:
                player: The player this store belongs to.
            """
            
            self._seeds = 0
            self._player = player
            
    class House(Storage):
        """Class implementing a kalaha house.
        
        The job of the house is to keep and give seeds.
        Seeds are deposited to the house by the ´plant´ method,
        and taken by the ´take´ metod.
        
        Class methods:
            __int__: Return the amount of seeds in the house as an int.
            __float__: Return the amount of seeds in the house as an float.
            __comp__: Compare the house to other storage units, or numeric types.
            __lt__: Is the seed count in this house lesser than 
               the one another storage or a numeric type.
            __gt__: Is the seed count in this house greater than 
               the one another storage or a numeric type.
            plant: Plant ´amount´ of seeds in the house from a hand.
            take: Return current seed-count, and reset seed-count to zero.
            get_seeds: Return the amount of seeds in the store.
            get_player: Return which player this house belongs to.
            get_index: Return the index of the house.
            reset: Reset the seed-count of the store to 0.
        """
        
        def __init__(self, seeds: int, player: int, index: int):
            """Initialize house instance with ´seed´ seeds.
            
            Args:
                seeds: The amount of seeds the house should start with.
                player: The player this house belongs to.
                index: The index of the store.
            """
            self._seeds = seeds
            self._player = player
            self._index = index
            
        def take(self) -> int:
            """Save seed-count, reset house and return saved seed-count."""
            seeds = self.get_seeds()
            self.reset()
            return seeds
            
        def get_index(self):
            """Return the index of the house."""
            return self._index
        
    def get_player_house_count(self) -> int:
        """Return the amount of houses for each player (same for both players)"""
        return self._player_houses
    
    def get_house_start_seeds(self) -> int:
        """Return the starting amount of seeds in each house (same for all houses)"""
        return self._house_seeds
        
    def get_house_seeds(self) -> int:
        """Return the sum of all seeds currently in houses.
        
        This is equal to self.get_house_start_seeds() - self._p1_store.get_seeds()
        """
        
        houses = self._houses
        seeds = [house.get_seeds() for house in houses]
        
        return sum(seeds)
        
    def get_store(self, player: int) -> int:
        """Return amount of seeds in specific store.
        
        Args:   
            player: Either a 1 or 2, representing p1 or p2 respectively.
        """
    
        if player == 1:
            return self._p1_store
        elif player == 2:
            return self._p2_store
            
    def get_valid_houses(self, player:  'int or None') -> [House]:
        """Return a list of valid houses (> 0 seeds) belonging to one or both players.
        
        Args:
            player: If None, returns houses rom both players, 
               else returns the player's houses.
        """
        
        houses = self.get_houses(player)
        
        return list(filter(lambda h: h.get_seeds() > 0, houses))
            
    def get_houses(self, player: 'int or None') -> [House]:
        """Return a list of houses belonging to one or both players
        
        Args:
            player: If None, returns all houses, else returns the player's houses.
        """
        
        if player is None:
            return self._houses
        
        if player == 1:
            return self._p1_houses
            
        elif player == 2:
            return self._p2_houses
        
    def get_house(self, player: int, index: int) -> int:
        """Return amount of seeds in specific house.
        
        Args:
            player: Either a 1 or 2, representing p1 or p2 respectively.
            index: The index of the store.
        """
        
        if player == 1:
            return self._p1_houses[index]
            
        elif player == 2:
            return self._p2_houses[index]
            
    def get_circuit(self, player: int, start_index: int) -> deque:
        """Return a deque of storage units that the player hand will move around.
        
        The deque comprises of the player's houses starting at
        ´start_index´, followed by the player's store, followed
        by the other player's houses and lastly the remaining player's stores.
        
        Args:
            player: Either a 1 or 2, representing p1 or p2 respectively.
            start_index: The start of the circuit.
        """
        
        if player == 1:
            h1 = self._p1_houses[start_index:]
            store = self._p1_store
            h2 = self._p2_houses
            h3 = self._p1_houses[:start_index]
            
        if player == 2:
            h1 = self._p2_houses[start_index:]
            store = self._p2_store
            h2 = self._p1_houses
            h3 = self._p2_houses[:start_index]
            
        queue_length = self.get_player_house_count() * 2 + 1
        
        circuit_list = h1 + [store] + h2 + h3
        return deque(circuit_list, maxlen=queue_length)
        
    def get_max_seeds(self) -> int:
        """Return the sum of all seeds."""
        return self._player_houses * 2 * self._house_seeds
        
    def get_scores(self) -> dict:
        """Return a dictionary of the scores of the two players."""
    
        scores = {}
        scores['player 1'] = self.get_store(1).get_seeds()
        scores['player 2'] = self.get_store(2).get_seeds()
        
        return scores
        
    def is_game_over(self) -> bool:
        """In its current possition, is the game over?"""
        
        return self.get_house_seeds() == 0
        
    def get_help_board(self) -> 'Board':
        """Return a board suited for helping.
        
        As the stores do not mater, their seed count will be zero.
        The seed count of a houses corrospond to their index.
        """
    
        help_board = Board(
           self._player_houses,
           self._house_seeds)
           
        for player in range(1, 3):
            houses = help_board.get_houses(player)
            for i in range(len(houses)):
                houses[i]._seeds = i
                
        return help_board
