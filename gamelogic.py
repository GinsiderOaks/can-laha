"""Module for keeping game logic"""

def plant_seeds( 
   player: int, 
   circuit: 'collections.deque', 
   wait: float=0,
   hand: int=0,
   ui: 'terminal.UI'=None):
    """Recursive method which plants the seeds according to the rules.
    
    The rules goes as follows:
        > The first house of the circuit is dequeued, and appenden to the back
        > If the hand is empty (0), the player attempts to take from the storage.
        > If the player fails because either the storage isn't theirs, or it's a store,
        the hand remains empty.
        > If the hand is still empty, because either the taking failed or the storage was empty,
        the loop is aborted and the player's turn ends.
        > If the hand isn't empty, it plants a seed in the storage and calls itself with the new
        circuit and hand values.
        
    Args:
        ui: The object responsible for displaying the user interface.
        player: Integer representation of the player playing.
        wait: The amount of time to pause.
        circuit: The circuit the hand will travel around until a break.
        hand: The amount of seeds in the hand.
    """
    
    # Overrides the (maybe) previously highlighted store.
    prev_storage = circuit[-1]
    
    if ui is not None:
        ui.board_drawer.draw_storage(prev_storage)
    
    # 'Shifts' the ciruit one to the left, and keeps the leftmost storage.
    # Same as calling circuit.rotate(-1), but that doesn't save the storage.
    storage = circuit.popleft()
    circuit.append(storage)
    
    # If the hand is empty,
    if hand == 0:
        # And the storage does not belong to the player,
        if storage.get_player() != player:
            # The turn is passed over.
            return
        
        # An attempt to take from the storage is made.
        try:
            hand = storage.take()
        except AttributeError:
            # On a fail the turn is passed over.
            return
            
        # If the storage was empty, the turn is passed over.
        if hand == 0:   
            return
            
    # If the hand is not empty, plants a seed from the hand.
    else:   
        hand = storage.plant(hand)
    
    if ui is not None:
        # Takes care of drawing the houses and stores.
        ui.board_drawer.draw_storage(storage, highlighted=True)
        ui.status_drawer.draw_hand(hand)
        # Waits for some time or user press
        ui.pause(wait)
    
    # Call itself with new parameters.
    plant_seeds(player, circuit, wait=wait, hand=hand, ui=ui)